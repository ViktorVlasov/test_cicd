from pytorch_lightning import Trainer
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks.model_checkpoint import ModelCheckpoint
from pytorch_lightning.callbacks import LearningRateMonitor
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from src.models.eff_net import EffNetClassifier

from src.configs.base_config import combine_config
from src.data.augmentations import DataAugmentations
from src.configs.augmentations_config import get_torchvision_transforms, \
                                             get_base_transforms, \
                                             train_augmentations, \
                                             val_augmentations, \
                                             test_augmentations
from src.data.data_loader import DistractedDriverDataset
import pytorch_lightning as pl
import os
import torch
import argparse
import warnings


def get_data_module(config: callable = None):
    base_transforms = get_torchvision_transforms(
        image_size=config.DATASET.IMG_SIZE
        )
    augmentations = DataAugmentations(
        base_transforms=base_transforms,
        train_augmentations=train_augmentations,
        val_augmentations=val_augmentations,
        test_augmentations=test_augmentations
        )
    # создаем DataModule-экземпляр
    data_module = DistractedDriverDataset(
        image_size=config.DATASET.IMG_SIZE,
        batch_size=config.DATASET.BATCH_SIZE,
        train_dir=config.DATASET.TRAIN_DIR,
        val_dir=config.DATASET.VAL_DIR,
        test_dir=config.DATASET.TEST_DIR,
        num_workers=config.DATASET.NUM_WORKERS,
        train_shuffle=config.DATASET.SHUFFLE_TRAIN,
        val_shuffle=config.DATASET.SHUFFLE_VAL,
        test_shuffle=config.DATASET.SHUFFLE_TEST,
        augmentations=augmentations
        )
    return data_module


def train(path_to_yaml: str = None,
          model_checkpoint: str = None):

    config = combine_config(path_to_yaml)
    os.makedirs(config.LOGGING.LOGS_PATH + '/' + config.LEARNING.EXPERIMENT)

    data_module = get_data_module(config=config)

    model = EffNetClassifier(eta=config.LEARNING.ETA,
                             model=config.LEARNING.EXPERIMENT)

    # список для отслеживания lr, ранних остановок, сохранения весов
    callbacks = [
        ModelCheckpoint(
            dirpath=config.CHECKPOINT.CKPT_PATH,
            filename=config.CHECKPOINT.FILENAME,
            save_top_k=config.CHECKPOINT.SAVE_TOP_K,
            monitor=config.CHECKPOINT.CKPT_MONITOR,
            mode=config.CHECKPOINT.CKPT_MODE
            ),
        LearningRateMonitor(
            logging_interval=config.LOGGING.LOGGING_INTERVAL
            ),
        EarlyStopping(
            monitor=config.ES.MONITOR,
            min_delta=config.ES.MIN_DELTA,
            patience=config.ES.PATIENCE,
            verbose=config.ES.VERBOSE,
            mode=config.ES.MODE
            )
        ]

    # указываем логгер
    logger = TensorBoardLogger(
        save_dir=config.LOGGING.LOGS_PATH,
        name=config.LEARNING.EXPERIMENT
        )

    # создаем трейнер - класс, который будет обучать
    trainer = Trainer(
        max_epochs=config.LEARNING.MAX_EPOCHS,
        logger=logger,
        callbacks=callbacks
        )

    if model_checkpoint:
        trainer.fit(model,
                    data_module,
                    ckpt_path=model_checkpoint)
    else:
        trainer.fit(model,
                    data_module,
                    ckpt_path=None)

    torch.save(model, config.LOGGING.SAVING_PATH)


def get_args_parser():
    """Функция для парсинга аргументов, указанных при вызове скрипта

    Returns:
        argparse.Namespace: объект, который содержит аргументы в виде атрибутов
    """
    parser = argparse.ArgumentParser(description="Training")
    parser.add_argument('--model_checkpoint',
                        default=None,
                        type=str,
                        help='Full path to model checkpoint')
    parser.add_argument("--config",
                        default='src/configs/default_config.yaml',
                        type=str)
    return parser.parse_args()


if __name__ == "__main__":
    # фиксируем сид
    SEED = 42
    pl.seed_everything(SEED)
    # отключаем избыточные варнинги
    warnings.filterwarnings("ignore")

    args = get_args_parser()

    train(args.config, args.model_checkpoint)
