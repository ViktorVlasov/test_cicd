import argparse
import torch
import pandas as pd

from pathlib import Path
from tqdm.autonotebook import tqdm
from sklearn.metrics import classification_report
from eff_net import EffNetClassifier

from src.data.data_loader import DistractedDriverDataset
from src.data.augmentations import DataAugmentations
from src.configs.base_config import combine_config
from src.configs.augmentations_config import get_torchvision_transforms, \
                                            train_augmentations, \
                                            val_augmentations, \
                                            test_augmentations


def get_data_module(batch_size: int, path_to_yaml: str):
    """Функция, собирающая data_module из собственного конфига,

    Args:
        batch_size (int): размер батча
        path_to_yaml (str): путь к конфигу

    Returns:
        DistractedDriverDataset: объект data_module
    """
    cfg = combine_config(path_to_yaml)

    base_transforms = get_torchvision_transforms(image_size=cfg.DATASET.IMG_SIZE)

    augmentations = DataAugmentations(base_transforms=base_transforms,
                                      train_augmentations=train_augmentations,
                                      val_augmentations=val_augmentations,
                                      test_augmentations=test_augmentations)

    data_module = DistractedDriverDataset(image_size=cfg.DATASET.IMG_SIZE,
                                          batch_size=batch_size,
                                          train_dir=cfg.DATASET.TRAIN_DIR,
                                          val_dir=cfg.DATASET.VAL_DIR,
                                          test_dir=cfg.DATASET.TEST_DIR,
                                          num_workers=cfg.DATASET.NUM_WORKERS,
                                          train_shuffle=cfg.DATASET.SHUFFLE_TRAIN,
                                          val_shuffle=cfg.DATASET.SHUFFLE_VAL,
                                          test_shuffle=cfg.DATASET.SHUFFLE_TEST,
                                          augmentations=augmentations)

    return data_module


def test(model,
         dataloader,
         name_experiment: str = 'default_experiment',
         device: str = 'cpu',
         save=True):
    """Функция для тестирования и валидации результатов модели

    Args:
        model: Используемая модель
        dataloader: test_dataloader или val_dataloader
        name_experiment (str): Название эксперимента
        save (bool): Сохранять результаты. По умолчанию: 'True'
                     Результаты тестирования/валидации будут сохранены
                     по пути attention_control/results/{name_experiment}.csv

    Returns:
        pandas.DataFrame:
                результат classification_report (precision, recall, f1-score, accuracy)

    """
    predictions = []
    targets = []

    model = model.to(device)
    model.eval()
    with torch.no_grad():
        for X, y in tqdm(dataloader):
            outputs = torch.argmax(model(X.to(torch.float32).to(device)), 1)
            predictions.append(outputs.detach())
            targets.append(y.detach())

    predictions = (torch.cat(predictions)).cpu().numpy()
    targets = (torch.cat(targets)).cpu().numpy()
    classes = dataloader.dataset.classes

    report = classification_report(targets, predictions,
                                   target_names=classes, output_dict=True)
    df_report = pd.DataFrame(report).transpose()
    if save:
        save_path = Path(__file__).parent / '../../results'
        if not save_path.exists():
            Path.mkdir(save_path, parents=True)
        output_file = name_experiment + '.csv'
        df_report.to_csv(save_path / output_file)

    return df_report


def get_args_parser():
    """Функция для парсинга аргументов, указанных при вызове скрипта

    Returns:
        argparse.Namespace: объект, который содержит аргументы в виде атрибутов
    """
    parser = argparse.ArgumentParser(description="Test model")
    parser.add_argument('--model',
                        default='epoch=3_valid_acc=0.78_valid_loss=1.06.ckpt',
                        type=str,
                        help='Model name in models dir')
    parser.add_argument("--config",
                        default='localpath_config.yaml',
                        type=str)
    parser.add_argument("-b", "--batch_size",
                        default=32,
                        type=int,
                        help="Number of images in a batch")
    parser.add_argument("--mode", default='test',
                        type=str,
                        help="Choosing a data loader: test or val Default: test")
    parser.add_argument("--device",
                        default="cpu",
                        type=str,
                        help="Device (Use cuda or cpu Default: cpu)")

    return parser.parse_args()


def get_init_params():
    """Функция для инициализации параметров функции test из аргументов,
    указанных при вызове скрипта.

    Returns:
        tuple: кортеж из параметров (model, dataloader, model_name, device)
    """
    args = get_args_parser()

    model_path = Path(__file__).parent / Path('../../models') / args.model
    path_to_yaml = Path(__file__).parent / '../configs' / args.config
    device = torch.device(args.device)
    mode = args.mode
    model = EffNetClassifier.load_from_checkpoint(checkpoint_path=model_path)
    bath_size = args.batch_size
    model_name = Path(args.model).stem

    dm = get_data_module(bath_size, path_to_yaml)
    dm.setup()

    if mode == 'test':
        dataloader = dm.test_dataloader()
    elif mode == 'val':
        dataloader = dm.val_dataloader()

    return model, dataloader, model_name, device


if __name__ == "__main__":
    model, dataloader, model_name, device = get_init_params()
    test(model, dataloader, model_name, device)
